# VENDING MACHINE
# Pillar Technology Kata
# Chase Schweitzer

import os
from decimal import Decimal

def clear():
	# Clear the terminal window
	os.system('cls' if os.name == 'nt' else 'clear')

def printline():
	# Print a seperator for the vending machine display
	print("=============================================")

class VendingMachine:
	prices = {"cola":Decimal("1.00"), "chips":Decimal("0.50"), "candy":Decimal("0.65")}

	def __init__(self):
		self.coins = Decimal('0.00')
		self.coin_return = []
		self.inventory = {"cola":1, "chips":1, "candy":1}
		self.dispensed = []
		self.flag = None # Used to display additional information like PRICE or THANK YOU
		self.change = {"quarter":1, "dime":1, "nickel":1} # The machine's stock of coins

	def display(self):
		# Display status of vending machine (inventory, coins, coin return, and any other messages)
		clear()

		print("============== VENDING MACHINE ==============")
		print(" (type coin name, item, 'exit', or 'return') ")

		printline()

		# Print the inventory
		for item in self.inventory.keys():
			print(item + ": " + str(self.inventory[item]))

		printline()


		# Print the coins
		print("coins: $" + str(self.coins))

		# Print the coin return
		if(self.coin_return == []):
			print("return: empty")
		else:
			print("return: " + ", ".join(self.coin_return))

		# Print the dispensed items
		if(self.dispensed == []):
			print("dispensed: empty")
		else:
			print("dispensed: " + ", ".join(self.dispensed))
	
		# Print any additional messages
		if(self.coins == Decimal('0.00')):
			printline()
			if(self.flag != None):
				print(self.flag + " // INSERT COIN")
			else:
				print("INSERT COIN")
		else:			
			if(self.flag != None):
				printline()
				print(self.flag)

		printline()

	def check(self, item):
		# Check if item can be purchased (adequate inventory, price, and change)
		if(self.inventory[item] > 0):
			if(self.coins >= self.prices[item]):
				if(self.return_coins(True)):
					self.dispensed.append(item)
					self.coins -= self.prices[item]
					self.inventory[item] -= 1
					self.flag = "THANK YOU"
					self.return_coins()
				else:
					self.flag = "EXACT CHANGE" # There aren't the coins to make change
			else:
				self.flag = "PRICE OF " + item.upper() + ": $" + str(self.prices[item]) # The amount entered is too low
		else:
			self.flag = item.upper() + " SOLD OUT" # The item isn't in stock

	def return_coins(self, check = False):
		# Return coins or check to make sure coins can be returned
		if(not check):
			while(self.coins != Decimal("0.00")):
				if(self.coins >= Decimal("0.25")):
					self.coin_return.append("quarter") # Add to the coin return
					self.change["quarter"] -= 1 # Decrement the change on hand
					self.coins -= Decimal("0.25") # Keep track of the amount to be returned

				elif(self.coins >= Decimal("0.10")):
					self.coin_return.append("dime")
					self.change["dime"] -= 1
					self.coins -= Decimal("0.10")

				elif(self.coins >= Decimal("0.05")):
					self.change["nickel"] -= 1
					self.coin_return.append("nickel")
					self.coins -= Decimal("0.05")

			return True
		else:
			# Make copies for testing purposes- don't actually change the machine
			check_coins = self.coins
			check_change = self.change
			while(check_coins != Decimal("0.00")):
				if(check_coins >= Decimal("0.25")):
					if(check_change["quarter"] == 0):
						return False
					else:
						check_change["quarter"] -= 1
						check_coins -= Decimal("0.25")
		
				elif(check_coins >= Decimal("0.10")):
					if(check_change["dime"] == 0):
						return False
					else:
						check_change["dime"] -= 1
						check_coins -= Decimal("0.10")
		
				elif(check_coins >= Decimal("0.05")):
					if(check_change["nickel"] == 0):
						return False
					else:
						check_change["nickel"] -= 1
						check_coins -= Decimal("0.05")

			return True
			


	def read(self):
		# Reads user input and prompts vending machine appropriately
		self.display()
		self.flag = None # Reset any additional messages

		user_input = input(">> ").lower()

		if(user_input == "penny"): # No switch statement in Python
			self.coin_return.append("penny")
			return True
		elif(user_input == "nickel"):
			self.coins += Decimal('.05')
			self.change["nickel"] += 1
			return True
		elif(user_input == "dime"):
			self.coins += Decimal('.10')
			self.change["dime"] += 1
			return True
		elif(user_input == "quarter"):
			self.coins += Decimal('.25')
			self.change["quarter"] += 1
			return True
		elif(user_input == "cola"):
			self.check("cola")
			return True
		elif(user_input == "chips"):
			self.check("chips")
			return True
		elif(user_input == "candy"):
			self.check("candy")
			return True
		elif(user_input == "return"):
			self.return_coins()
			return True
		elif(user_input == "exit"):
			return False
		else:
			return True

vm = VendingMachine()
vm.display()

running = True
while(running): # Run the vending machine until user types 'exit'
	running = vm.read()

clear()